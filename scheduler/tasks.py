from celery.decorators import task
from celery.utils.log import get_task_logger
from django.apps import apps
import pexpect
import random
import pytsk3

logger = get_task_logger(__name__)


@task(name="send_feedback_email_task")
def send_feedback_email_task():
    """sends an email when feedback form is filled successfully"""
    logger.info("Sent feedback email")
    logger.info([installedApp.path for installedApp in apps.get_app_configs()])
    return "win"

@task(name="extract_registry_hives_from_windows_image")
def extract_registry_hives_from_windows_image(imageFile):
    print("t")
    return True

def isEncaseFile(imageFile):
    '''
    Returns true if file ends in .E01
    '''
    return imageFile.lower().endswith(('.e0', '.e01'))

def mountEWF(imageFile):
    '''
    Mounts an imagefile using mount_ewf.py, returns mount point
    '''
    ewfMountPoint = "/mnt/temp_forensicated_ewf_" + str(random.randint(0000000000,9999999999))
    makeDirectory = pexpect.spawn('mkdir {}'.format(ewfMountPoint))
    mountEWF = pexpect.spawn('mount_ewf.py {} {}'.format(imageFile, ewfMountPoint))
    print(mountEWF.expect(pexpect.EOF))
    print(mountEWF.read())
    print(str(mountEWF))
    return ewfMountPoint


def getAllocatedPartitionMetadata(imageFile):
    '''
    Gets metadata about allocated partitions, such as the starting
    byte offset and the filesystem format type
    '''

    partitionOffsets = []

    image = pytsk3.Img_Info(imageFile) # Open Image
    volume = pytsk3.Volume_Info(image) # Open Volume
    blockSize = int(volume.info.block_size) # Determine volume block size

    # Iterate the allocated partitions in the volume, get offset
    for partition in volume:

        # Only get info for allocated partitions
        if(str(partition.flags) == "TSK_VS_PART_FLAG_ALLOC"):

            # Byte offset where partition starts
            partitionStartingOffset = int(partition.start) * blockSize

            # Open the filesystem
            fileSystem = pytsk3.FS_Info(image, partitionStartingOffset, pytsk3.TSK_FS_TYPE_NTFS)

            # Get the filesystems long name
            fileSystemTypeLongName = str(fileSystem.info.ftype)

            #TODO also map it to short name (ie TSK_FS_TYPE_NTFS -> ntfs), i think this might be useful for plaso 
            partitionOffsets.append({'startingByteOffset' : partitionStartingOffset, 'fileSystemFormatLong' : fileSystemTypeLongName})

    return partitionOffsets


print(getAllocatedPartitionMetadata('/mnt/ewf/ewf1'));
#print(isEncaseFile('/media/sf_silen/Downloads/nps-2008-jean.E01'))
#print(mountEWF('/media/sf_silen/Downloads/nps-2008-jean.E01'))
