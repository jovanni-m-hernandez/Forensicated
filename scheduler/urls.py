# scheduler/urls.py

from django.conf.urls import url

from scheduler import views

urlpatterns = [
  url(r'^$', views.index, name='index'), # Index 
  url(r'^new/', views.new, name='new'), # Create a new job
  url(r'^job/', views.job, name='job'), # View an existing job
]