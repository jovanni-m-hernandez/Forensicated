@task(name="send_feedback_email_test")
def send_feedback_email_test():
    """sends an email when feedback form is filled successfully"""
    logger.info("Sent feedback email")
    logger.info([installedApp.path for installedApp in apps.get_app_configs()])
    return "win"
