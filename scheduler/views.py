from django.shortcuts import render, HttpResponse
from scheduler import tasks

# Create your views here.

def index(request):
    return render(request, 'index.html')

	
'''
Page for creating a new job of tasks
'''	
def new(request):
    return render(request, 'new.html')

	
'''
Page for vuewing a job of tasks.
TODO, if post, create new job from post vars, if get w/
job number, show status
'''	
def job(request):
    print("test")
    print(tasks.send_feedback_email_task.delay())
    return render(request, 'job.html')
